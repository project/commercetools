INTRODUCTION
------------

This module enables the user to request products via a
Commercetools API.


REQUIREMENTS
------------

Two packages are required:

* commercetools/php-sdk
* cache/filesystem-adapter

When you install the module with composer, these packages will be
automatically downloaded.


INSTALLATION
------------

Install this module with composer:

`composer require drupal/commercetools`

When you do this, the dependencies will automatically be downloaded.

After this, enable the module at /admin/modules.


CONFIGURATION
-------------

After enabling the module, you should configure a client in the
sites/default/settings.php file. Paste the following code in the
settings file:

```
$settings['commercetools'] = [
  'client_id' => '',
  'client_secret' => '',
  'project' => '',
];
```

Fill in the three values. After that, an overview of the products
can be seen at /products. Of course, you need to make sure that
some products are created in the Commercetools Merchant Center.

If you currently don't have a project at the Commercetools Merchant
Center, you can create a temporary project at https://mc.commercetools.com.
This project will be available for 60 days. Not enough for production
but enough to give it a try :)


NOTES
-----

* The Symfony SDK can also be used for this instead of the PHP SDK.
Maybe this will be done in a future release.
