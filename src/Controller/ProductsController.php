<?php

namespace Drupal\commercetools\Controller;

use Commercetools\Core\Client;
use Commercetools\Core\Config;
use Commercetools\Core\Model\Common\Context;
use Commercetools\Core\Request\Products\ProductProjectionQueryRequest;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Site\Settings;

class ProductsController extends ControllerBase {

  public function buildPage() {
    // Setup client from settings.
    $commercetools_settings = Settings::get('commercetools');
    $config = Config::fromArray([
      'client_id' => $commercetools_settings['client_id'],
      'client_secret' => $commercetools_settings['client_secret'],
      'project' => $commercetools_settings['project'],
    ]);
    $context = Context::of()->setLanguages(['en'])->setLocale('en_US')->setGraceful(true);
    $config = $config->setContext($context)->setThrowExceptions(true);
    $client = Client::ofConfig($config);

    // Do the request.
    $request = ProductProjectionQueryRequest::of();
    try {
      $response = $request->executeWithClient($client);
      $object_with_products = $request->mapResponse($response);
    }
    catch (\Exception $exception) {
      $object_with_products = [];

      // Log error messages to watchdog.
      $this->getLogger('commercetools')->error($exception->getMessage());
    }

    // Put products in variable.
    $products = [];
    foreach ($object_with_products as $product) {
      $image_url = count($product->getMasterVariant()->getImages()) > 0 ? $product->getMasterVariant()->getImages()->current()->getUrl() : "http://placehold.it/165.png";
      $products[] = [
        'name' => $product->getName()->en_US,
        'image_url' => $image_url,
      ];
    }

    return [
      '#theme' => 'products',
      '#products' => $products,
    ];
  }

}
